VNWA on Linux
=============
Last updated: 2020-07-12

Author: Jan Zelenkaa

Tested Against:

* VNWA version 36.7.9.0.[^fn1]
* Wine version 5.11,[^fn2] should work from version 2.8 (not tested).[^fn3]
* Wine-mono version 5.0.0.[^fn4]
* Linux kernel version 5.7.6.


As the last description of the installation procedure is rather outdated
(2014).[^fn5] I present here a newer version of installation procedure based on
previous one. I have tested it solely against FA-VA5 instrument[^fn6] on
Archlinux[^fn7] operation system. The goal is to provide simple setup which
brings core functionality with minimal user effort.

## USB privileges configuration
You need to add yourself to groups which governs USB access. For Archlinux[^fn7]:
```
# usermod -aG uucp,lock <your-username>
```

Where <i>\<your-username\></i> is the name of non-root user you want to use.
The names of the groups differ in other Linux distros. Please refer to your
Linux distro webpages for Group-management, or "accessing serial" wiki pages.

After this step, it is crucial to completely log-off and log-in so the new
groups takes action. If unsure, reboot the PC.

## Wine installation

Install wine[^fn2] and wine-mono[^fn4] packages.

For Archlinux:

enable multilib in <i>/etc/pacman.conf</i> by un-commenting these lines as
administrator:

```
[multilib]
Include = /etc/pacman.d/mirrorlist
```

Refresh package database, update os, install <b>wine</b>[^fn2] and
<b>wine-mono</b>[^fn4]:

```
# pacman -Syu
# pacman -S wine wine-mono
```

## VNWA installation

Download <b>VNWA</b>[^fn1]:
```
$ curl "https://www.sdr-kits.net/DG8SAQ/vnwaupdate.php?path=installer" > VNWA-installer.exe
```

Or:
```
$ wget "https://www.sdr-kits.net/DG8SAQ/vnwaupdate.php?path=installer"
```

Install <b>VNWA</b>[^fn1]:
```
$ wine VNWA-installer.exe
```
If you see message that wine-mono is not installed, kill the installation
procedure and re-run it. Message should not show-up again. If the installation
runs, do not get stressed by error output in the console.

During installation:

- Un-check the "create desktop icon" checkbox.

- It is unnecessary to install the certificate.

If you do not un-check the "create desktop icon" checkbox, installation itself will
produce some error output like <i>"co-create instance failed -> did not
installed links everywhere"</i>, do not get stressed by it.

Test the installation (presumes you've installed into default location):
```
$ wine ~/.wine/drive_c/VNWA/VNWA.exe
```

After few seconds of hesitation and error output into console it should start a
VNWA[^fn1] instance.

## COM-port configuration
Wine supports nowadays forwarding of serial ports to Linux.[^fn3] To hard-link
the serial port to specific COM port you must edit <b>regedit</b>[^fn8]:
```
$ wine regedit
```
In <i>HKEY_LOCAL_MACHINE\Software\Wine\Ports</i> append (right-click ->
<i>New</i>) <i>String Value</i> with <b>name</b> <i>COM1</i> and <b>Value
data</b> <i>/dev/ttyUSB0</i>. This presumes that the FA-VA5[^fn6] is the only
connected device with the serial port. In other cases, use appropriate USB
port,[^fn9] or set-up udev-rules.[^fn10]

## Enjoy
At this point, you should have working instance of VNWA[^fn1] on your Linux
distribution. Test it by (if you installed into default location):
```
$ wine ~/.wine/drive_c/VNWA/VNWA.exe
```
and try to connect to your FA-VA5[^fn6] instrument.

## Conclusin
If you see any errors or missing description in this documentation, do not
hesitate to contact me. The guide provides fast and simple way how to connect
to your FA-VA5[^fn6] in Linux. It does not aim to be exhaustive. Refer to the
original guide for further refinements.[^fn5]

## Citations
[^fn1]: https://www.sdr-kits.net/DG8SAQ-VNWA-software-documentation-user-guide

[^fn2]: https://www.winehq.org/

[^fn3]: https://wiki.winehq.org/Wine_User's_Guide#Serial_and_Parallel_Ports

[^fn4]: https://wiki.winehq.org/Mono

[^fn5]: http://www.sdr-kits.net/DG8SAQ/VNWA/VNWA_Linux_Package.zip

[^fn6]: https://www.sdr-kits.net/VA5_Page

[^fn7]: https://www.archlinux.org/

[^fn8]: https://superuser.com/questions/1477796/making-usb-work-in-wine-4-0-ubuntu-19-04

[^fn9]: <i>ls /dev/ttyUSB*</i> to find the possible names.

[^fn10]: https://wiki.archlinux.org/index.php/Udev#About_udev_rules
